extends Node2D

var nb_question = 1
var current_text_woman = 0
var current_text_man = 1
var i = 0
var text_man = [
#Noeud A --> 0
"Vous allez bien ?\nRien de cassé ?",
"Oui heureusement !",
"Que s'est-il passé pour que l'on jette votre sac ?", #question 1
"Comment vous appelez-vous ?", #question 1
"Bonne soirée... (s'en aller)", #question 1
#noeud B --> 5
"Antoine, enchanté !",
"Que fait une femme comme vous dans un endroit pareil ?", #question 2
"Que s'est-il passé pour que l'on jette votre sac ?", #question 2
"Vous voulez prendre un verre avec moi ?", #question 2
#Noeud C --> 9
"Dites m'en plus, que s'est-il passé ?", #question 3
"Rien de trop grave j'espère ?", #question 3
"Vous êtes sûre que ce sont vos amis ?",  #question 3
#Noeud F --> 12
"Et ce sont vos amis qui l'ont lancé comme ça ?", #question 4
"Et où sont-il ?", #question 4
"Je ne suis pas sûr qu'on puisse les appeler des amis...", #question 4
#Noeud D --> 15
"Etait ?,\nvous vous séparez ?",
"Et bien, bon courage ! (s'en aller)", #question 5
"Quelle coincidence, j'en cherche un également", #question 5
"Vous en avez du courage", #question 5
#Noeud H --> 19
"Excusez-moi, je m'inquiétais juste pour vous", #question 6
"Excusez-moi, restons en là", #quesiton 6
"Je ne faisais que poser une question", #question 6
#Noeud J --> 22
"Excusez-moi", #question 7
"Nous nous sommes mal compris", #question 7
"Au revoir", #question 7
#Noeud I --> 25
"Je suis également parti à la recherche d'un foyer", #question 8
"Vous en avez du courage !",  #question 8
"Je devrais peut-être repartir chercher le mien", #question 8
#Noeud E --> 28
"Peut-être pourrions nous en discuter autour d'un verre", #question 9
"Vous avez peut-être raison, bon courage à vous alors", #quesion 9
"Si vous le dites", #question 9
#Noeud G --> 31
"En effet, j'ai du me résoudre à quitter mon emploi aujourd'hui", #question 10
"Vous avez raison", #question 10
"Je ne suis absolument pas d'accord (s'en aller)", #question 10
#Noeud K --> 34
"Discutons plus amplement", #question 11
"Que proposez-vous", #question 11
"Peut-être pas... (se prend une baffe)", #question 11
#Noeud L --> 37
"Discuter plus amplement autours d'un verre ?", #question 12
"Tout d'abord recommencer sur de bonnes bases", #question 12
"(Laisser tomber)", #question 12
#Noeud M --> 40
"Voudriez-vous\nvous assoir\nau comptoir pour\nboire un verre?"

]
var text_woman = [
#Noeud A --> 0
"Je vais bien merci\n Et vous ?",
#Noeud B --> 1
"Lucie, et vous ?",
#Noeud C --> 2
"J'étais avec des amis,\nmais ça a dérappé",
#Noeud F --> 3
"J'étais venue\navec des amis",
#Noeud D --> 4
"Et bien pour tout vous dire,\nl'un d'eux était mon fiancé...",
"Ce n'est pas très grave.\nJe ne me sentais plus chez moi dans notre foyer",
"Il va falloir que\nj'en construise\nun qui me\nconvient mieux",
#Noeud H --> 7
"Je ne suis pas sûre que\nvous soyez bien placé\npour juger mes amis",
#Noeud J --> 8
"Et cette dernière\nétait déplacée !",
#Noeud I --> 9
"Disons que j'ai\ncompris que ce foyer\nn'était pas celui\nqu'il me fallait",
#Noeusd E --> 10
"ce n'est pas évident\nde tout changer\ndu jour au lendemain",
"ça demande du courage\nd'abandonner tout ce\nqu'on connait même si l'on\nsait que ça ne\nnous correspond pas",
#Noeud G --> 12
"Et bien je n'avais\npas vraiment le choix",
#Noeud K --> 13
"Nous avons peut-être\nquelques points communs",
#Noeud L --> 14
"Que vouliez-vous faire ?",
#Noeud M --> nul
]

var next = [1,0,2, #A
1,0,2, #B --> 3
1,2, #C --> 6
1,2, #F --> 8
1,0,1,1,2, #D --> 10
1,2, #H --> 15
1,2, #J --> 17
1,2, #I --> 19
1,1,2, #E --> 21
1,2, #G --> 24
1,2, #K --> 26
1,2, #L --> 28
0 #M --> 30
]

func _ready():
	$blablah_man.hide()
	$blablah_woman.hide()
	$Love.play()

func _on_time_lecture_timeout():
	$blablah_man/Speach_man.rect_global_position = Vector2(1160, 125)
	$blablah_woman/Speach_woman.rect_global_position = Vector2(150, 125)
	$blablah_man/Speach_man.rect_size = Vector2(1300, 900)
	$blablah_woman/Speach_woman.rect_size = Vector2(1300, 900)
	if next[i] == 0:
		$blablah_man/Speach_man.text = text_man[current_text_man]
		current_text_man += 1
		if i == 30:
			amis()
		i += 1
	elif next[i] == 1:
		$blablah_woman/Speach_woman.text = text_woman[current_text_woman]
		current_text_woman += 1
		i += 1
	else:
		$blablah_man/Speach_man.text = ""
		$blablah_man/Answer_A.show()
		$blablah_man/Answer_A.text = text_man[current_text_man]
		$blablah_man/Answer_B.show()
		$blablah_man/Answer_B.text = text_man[current_text_man + 1]
		$blablah_man/Answer_C.show()
		$blablah_man/Answer_C.text = text_man[current_text_man + 2]
		yield()
	if i < next.size():
		$time_lecture.start()

func _on_Answer_A_pressed():
	var y = _on_time_lecture_timeout()
	if nb_question == 1: #A --> C
		current_text_woman = 2
		current_text_man = 9
		i = 6
		nb_question = 3
	elif nb_question == 2: #B --> F
		current_text_man = 12
		current_text_woman = 3
		i = 8
		nb_question = 4
	elif nb_question == 3: #C --> I
		current_text_man = 25
		current_text_woman = 9
		i = 19
		nb_question = 8
	elif nb_question == 4: #F --> D
		current_text_man = 15
		current_text_woman = 4
		i = 10
		nb_question = 5
	elif nb_question == 5: #D --> rien
		rateau()
		i = next.size()
	elif nb_question == 6: #H --> I
		current_text_man = 25
		current_text_woman = 9
		i = 19
		nb_question = 8
	elif nb_question == 7: #J --> I
		current_text_man = 25
		current_text_woman = 9
		i = 19
		nb_question = 8
	elif nb_question == 8: #I --> E
		current_text_man = 28
		current_text_woman = 10
		i = 21
		nb_question = 9
	elif nb_question == 9: #E --> rien
		famille()
		i = next.size()
	elif nb_question == 10: #G --> K
		current_text_man = 34
		current_text_woman = 13
		i = 26
		nb_question = 11
	elif nb_question == 11: #K --> rien
		famille()
		i = next.size()
	elif nb_question == 12: #L --> M
		current_text_man = 40
		current_text_woman = text_woman.size()
		i = 30
	elif i != next.size():
		$time_lecture.start()
	$blablah_man/Answer_A.hide()
	$blablah_man/Answer_B.hide()
	$blablah_man/Answer_C.hide()
	y.resume()
	
func _on_Answer_B_pressed():
	var y = _on_time_lecture_timeout()
	if nb_question == 1: #A --> B
		current_text_man = 5
		current_text_woman = 1
		i = 3
		nb_question = 2
	elif nb_question == 2: #B --> C
		current_text_man = 9
		current_text_woman = 2
		i = 6
		nb_question = 3
	elif nb_question == 3: #C --> D
		current_text_man = 15
		current_text_woman = 4
		i = 10
		nb_question = 5
	elif nb_question == 4: #F --> H
		current_text_man = 19
		current_text_woman = 7
		i = 15
		nb_question = 6
	elif nb_question == 5: #D --> E
		current_text_man = 28
		current_text_woman = 10
		i = 21
		nb_question = 9
	elif nb_question == 6: #H --> rien
		rateau()
		i = next.size()
	elif nb_question == 7: #J --> L
		current_text_man = 37
		current_text_woman = 14
		i = 28
		nb_question = 12
	elif nb_question == 8: #I --> G
		current_text_man = 31
		current_text_woman = 12
		i = 24
		nb_question = 10
	elif nb_question == 9: #E --> rien
		rateau()
		i = next.size()
	elif nb_question == 10: #G --> M
		current_text_man = 40
		current_text_woman = text_woman.size()
		i = 30
	elif nb_question == 11: #K --> rien
		famille()
		i = next.size()
	elif nb_question == 12: #L -->M
		current_text_man = 40
		current_text_woman = text_woman.size()
		i = 30
	if i != next.size():
		$time_lecture.start()
	$blablah_man/Answer_A.hide()
	$blablah_man/Answer_B.hide()
	$blablah_man/Answer_C.hide()
	y.resume()

func _on_Answer_C_pressed():
	var y = _on_time_lecture_timeout()
	if nb_question == 1: #A --> fin
		rateau()
		i = next.size()
	elif nb_question == 2: #B --> F
		current_text_man = 12
		current_text_woman = 3
		i = 8
		nb_question = 4
	elif nb_question == 3: #C --> H
		current_text_man = 19
		current_text_woman = 7
		i = 15
		nb_question = 6
	elif nb_question == 4: #F --> H
		current_text_man = 19
		current_text_woman = 7
		i = 15
		nb_question = 6
	elif nb_question == 5: #D --> G
		current_text_man = 31
		current_text_woman = 12
		i = 24
		nb_question = 10
	elif nb_question == 6: #H --> J
		current_text_man = 22
		current_text_woman = 8
		i = 17
		nb_question = 7
	elif nb_question == 7: #J --> rien
		rateau()
		i = next.size()
	elif nb_question == 8: #I --> rien
		rateau()
		i = next.size()
	elif nb_question == 9: #E --> rien
		famille()
		i = next.size()
	elif nb_question == 10: #G --> rien
		rateau()
		i = next.size()
	elif nb_question == 11: #K --> rien
		rateau()
		i = next.size()
	elif nb_question == 12: #L --> rien
		rateau()
		i = next.size()
	if i != next.size():
		$time_lecture.start()
	$blablah_man/Answer_A.hide()
	$blablah_man/Answer_B.hide()
	$blablah_man/Answer_C.hide()
	y.resume()
	
func famille():
	$Music.stop()
	$EndFamily.play()
	
func amis():
	$Music.stop()
	get_tree().change_scene("res://src/game/Main.tscn")
	
func rateau():
	$Music.stop()
	$Woman.queue_free()
	$blablah_man.queue_free()
	$blablah_woman.queue_free()
	$EndClodo.play()
	

func _on_EndClodo_finished():
	get_tree().quit()

func _on_EndFamily_finished():
	get_tree().quit()


func _on_Love_finished():
	$blablah_man.show()
	$blablah_woman.show()
	$Music.play()
	$blablah_man/Speach_man.text = text_man[0]
	$blablah_man/Answer_A.hide()
	$blablah_man/Answer_B.hide()
	$blablah_man/Answer_C.hide()
	$time_lecture.start()
