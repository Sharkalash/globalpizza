extends Area2D


const SPEED = 1000

var dab = 0
var a = []

func _ready():
	pass

func _process(delta):
	var velocity = Vector2()
	var rot = 0
	
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_page_up"):
		rot -= 1
	if Input.is_action_pressed("ui_page_down"):
		rot += 1
	
	velocity = velocity.normalized()
	velocity *= SPEED
	position += velocity * delta
	
	if(position.x < 0):
		position.x = 0
	
	if rot < 0:
		$AnimatedSprite.scale.x = 1
		$AnimatedSprite.frame = 1 + dab
	elif rot > 0:
		$AnimatedSprite.scale.x = -1
		$AnimatedSprite.frame = 1 + dab
	else:
		$AnimatedSprite.scale.x = 1
		$AnimatedSprite.frame = rot

func _on_AnimatedSprite_frame_changed():
	match $AnimatedSprite.frame:
		0:
			$Collision0.disabled = false
			$Collision1.disabled = true
			$Collision2.disabled = true
		1:
			$Collision0.disabled = true
			$Collision1.disabled = false
			$Collision2.disabled = true
		2:
			$Collision0.disabled = true
			$Collision1.disabled = true
			$Collision2.disabled = false
	
	$Collision1.scale.x = $AnimatedSprite.scale.x
	$Collision2.scale.x = $AnimatedSprite.scale.x

func _on_Player_area_entered(area):
	a.append(area)

func _on_Player_area_exited(area):
	a.erase(area)
	
func isColliding(area):
	for i in a:
		if(area == i):
			return true
	
	return false
