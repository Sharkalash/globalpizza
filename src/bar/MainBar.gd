extends Node2D


var Beer = preload("res://src/bar/Beer.tscn")
var Bottle = preload("res://src/bar/Bottle.tscn")
var Bottle2 = preload("res://src/bar/Bottle2.tscn")
var Pizza = preload("res://src/bar/Pizza.tscn")
var Coke = preload("res://src/bar/Coke.tscn")
var Handbag = preload("res://src/bar/Handbag.tscn")

var Projectiles = [Handbag, Beer, Bottle, Bottle2, Pizza, Coke]
var handbag_spawned = 0

func _ready():
	pass

func _on_Timer_timeout():
	$AnimationPlayer.play("PitchRaise")


func _on_AnimationPlayer_animation_finished(anim_name):
	$SpawnProjectile.start()


func _on_SpawnProjectile_timeout():
	if randi() % 5 == 0:
		var i = randi() % (6 - handbag_spawned) + handbag_spawned
		if i == 0:
			handbag_spawned = 1
		var c = Container.new()
		c.rect_position = Vector2((randi() % 1720) + 100, 1040)
		add_child(c)
		var p = Projectiles[i].instance()
		p.get_node("AnimationPlayer").playback_speed = randf() + 0.8
		p.connect("touch", self, "_on_projectile_touch", [p])
		c.add_child(p)


func _on_projectile_touch(p):
	if(p.name == "Handbag"):
		get_tree().change_scene("res://src/super_seducer/Super_seducer.tscn")
	else:
		$TextureProgress.value += 10


func _on_TextureProgress_value_changed(value):
	if value > 80:
		$TextureProgress/Particles2D.emitting = true
		$Player.dab = 1


func _on_AudioStreamPlayer_finished():
	if $TextureProgress.value < 80:
		get_tree().change_scene("res://src/game/Main.tscn")
	else:
		$EndClodo.play()

func _on_EndClodo_finished():
	get_tree().quit()