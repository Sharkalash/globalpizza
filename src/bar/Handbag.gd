extends Area2D

signal touch

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Throw":
		if get_parent().get_parent().get_node("Player").isColliding(self):
			$AnimationPlayer.play("Touch")
			emit_signal("touch")
		else:
			$AnimationPlayer.play("FadeOut")
	else:
		queue_free()