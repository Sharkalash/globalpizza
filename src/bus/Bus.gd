extends Area2D

var size
var started = false
var speed = 500;
var collide = false
var collidingObject
var y = 3

signal start
signal pick
signal nonpick

# Called when the node enters the scene tree for the first time.
func _ready():
	size = get_viewport().size;
	set_process(false)
	 # Replace with function body.


func _process(delta):
	if(started):
		if(randi() % 20 == 2):
			position.y += y
			y *= -1
	else:
		if((position.x + $BusSprite.texture.get_width() / 2) >= speed * 1.5):
			emit_signal("start")
			started = true
		else:
			position.x += speed * delta
	
	
	

func _input(event):
	if((event.as_text() == "W" or event.as_text() == "X") and event.is_pressed() and !event.is_echo()):
		if(collide):
			collidingObject.queue_free()
			emit_signal("pick")
		else:
			emit_signal("nonpick")
		


func _on_Bus_area_entered(area):
	collide = true
	collidingObject = area
	area.get_node("PedestrianSprite").animation = "up"
	area.position.y += 30 * get_process_delta_time()
	

func _on_Bus_area_exited(area):
	collide = false
	if(area != null):
		area.get_node("PedestrianSprite").animation = "angry"
		


func _on_MainBus_start_bus():
	set_process(true)

func _on_Bus_area_shape_entered(area_id, area, area_shape, self_shape):	
	
	if(self_shape == 1):
		area.scale.x *= 1.5
		area.scale.y *= 1.5
		area.position.y -= 10
	else:
		collide = true
		collidingObject = area
		area.get_node("PedestrianSprite").animation = "up"
		area.position.y -= 30 * get_process_delta_time()


func _on_Bus_area_shape_exited(area_id, area, area_shape, self_shape):
	if(self_shape != 0):
		collide = false
		if(area != null):
			area.scale.x /= 2
			area.scale.y /= 2
			area.get_node("PedestrianSprite").animation = "angry"