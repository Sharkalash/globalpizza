extends Area2D

var speed = 500

func _ready():
	set_process(false)

func _process(delta):
	position.x -= delta * speed
	
	if($PedestrianSprite.animation == "up"):
		position.y -= delta * 50
	
	if(position.x < 0):
		queue_free()
	
func _on_Start():
	set_process(true)