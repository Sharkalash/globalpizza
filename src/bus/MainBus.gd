extends Node2D

signal start_pedestrian
signal start_road
signal start_bus

var Pedestrian = preload("res://src/bus/Pedestrian.tscn")
var nb_pedestrian = 0
var nb_pedestrian_picked = 0

func createPedestrian(position):
	var p = Pedestrian.instance()
	add_child(p)
	p.position = position
	connect("start_pedestrian",p,"_on_Start")
	nb_pedestrian += 1
	
	
func createRythm(r,x,y, nb):
	for i in range(nb):
		createPedestrian(Vector2(x,y))
		x +=  float(r)
	
	return float(x)

func createAllPedestrian():
	var p = Pedestrian.instance()
	var y = 1080 - p.get_node("PedestrianSprite").frames.get_frame("normal",0).get_height() - 100
	var x = p.speed * 2.2
	var tempo = 140
	var noire = float((float(60) / tempo)) * float(p.speed)
	var rythm = [float(4)*noire, float(2)*noire, noire, noire/float(2), noire/float(4)]
	
	x = createRythm(rythm[2],x,y,10)
	x = createRythm(rythm[3],x,y,2)
	x = createRythm(rythm[2],x,y,3)
	x = createRythm(rythm[3],x,y,4)	

	for i in range(3):
		x = createRythm(rythm[2],x,y,1)
		x = createRythm(rythm[3],x,y,2)

	for i in range(3):
		for j in range(4):
			x = createRythm(rythm[3],x,y,2)
			x = createRythm(rythm[3],x,y,2)
			x = createRythm(rythm[2],x,y,2)
		
		for j in range(4):
			x = createRythm(rythm[3],x,y,2)
			x = createRythm(rythm[4],x,y,4)
			x = createRythm(rythm[1],x,y,1)
			
		for j in range(4):
			x = createRythm(rythm[3],x,y,2)
			x = createRythm(rythm[4],x,y,2)
			x = createRythm(rythm[3],x,y,1)
			x = createRythm(rythm[2],x,y,1)
			x = createRythm(rythm[3],x,y,2)
		
		for j in range(4):
			x = createRythm(rythm[3],x,y,4)
			x = createRythm(rythm[2],x,y,1)
			x = createRythm(rythm[3],x,y,2)
			x = createRythm(rythm[4],x,y,2)
	
	for j in range(4):
		x = createRythm(rythm[3],x,y,2)
		x = createRythm(rythm[3],x,y,2)
		x = createRythm(rythm[2],x,y,2)
		
	for j in range(5):
		x = createRythm(rythm[3],x,y,2)
		x = createRythm(rythm[4],x,y,4)
		x = createRythm(rythm[1],x,y,1)
		
	x = createRythm(rythm[3],x,y,8)
	x = createRythm(rythm[2],x,y,8)
	x = createRythm(rythm[1],x,y,2)
	
	p.queue_free()

func _ready():
	randomize()
	createAllPedestrian()
	
	$Music.play()
	connect("start_road", $RoadSprite, "_on_MainBus_start_road")
	connect("start_road", $RoadSprite2, "_on_MainBus_start_road")
	connect("start_road", $RoadSprite3, "_on_MainBus_start_road")

func _on_Bus_start():
	emit_signal("start_pedestrian")
	emit_signal("start_road")
	$Label.queue_free()
	
	 # Replace with function body.


func _on_StartTimer_timeout():
	emit_signal("start_bus")

func _on_Bus_pick():
	nb_pedestrian_picked += 1

func _on_Music_finished():
	$Bus.queue_free()
	
	var ratio = float(nb_pedestrian_picked) / float(nb_pedestrian)
	
	$Score.text = str(int(ratio * 100)) + "%"
	$Score.rect_position = Vector2((get_viewport().size.x - $Score.get_rect().size.x)/2, 0)
	$ScoreTimer.start()
	get_tree().set_meta("scoreBus", int(ratio * 100))

func _on_Bus_nonpick():
	if(nb_pedestrian_picked > 0):
		nb_pedestrian_picked -= 1


func _on_ScoreTimer_timeout():
	get_tree().change_scene("res://src/game/Main.tscn")