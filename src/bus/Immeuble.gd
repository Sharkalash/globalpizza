extends Sprite

var speed = 30

func _ready():
	set_process(false)
	
func _process(delta):
	position.x -= delta * speed



func _on_Bus_start():
	set_process(true)
