extends PathFollow2D

export (int) var speed

signal flotte_destroyed

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	offset += speed * delta
		

func _on_Vaisseau_tree_exited():
	if get_child_count() == 0:
		emit_signal("flotte_destroyed")
		get_parent().get_parent().queue_free()