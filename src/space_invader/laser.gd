extends RigidBody2D


func set_pos(pos):
	position = pos


func _on_laser_body_entered(body):
	body.touched()
	hide()
	$CollisionShape2D.call_deferred("set", "disabled", true)
	yield($AudioStreamPlayer, "finished")
	queue_free()


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
