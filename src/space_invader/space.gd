extends Sprite

# Declare member variables here. Examples:
# var a = 2
var flotte = preload("flote_enemie.tscn")
var kill = 0
var nb_flotte = 0
const NB_FLOTTE = 3

# Called when the node enters the scene tree for the first time.
func _ready():
	_on_inter_wave_timeout()


func _on_flotte_destroyed():
	kill += 1
	print(kill)
	if kill == NB_FLOTTE:
		get_tree().set_meta("travel",true)
		get_tree().change_scene("res://src/game/Main.tscn")


func _on_inter_wave_timeout():
	if nb_flotte < NB_FLOTTE:
		nb_flotte += 1
		var flotte_active = flotte.instance()
		add_child(flotte_active)
		flotte_active.get_node("Path2D/PathFollow2D").connect("flotte_destroyed", self, "_on_flotte_destroyed")