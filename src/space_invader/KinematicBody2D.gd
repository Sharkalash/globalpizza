extends KinematicBody2D


export (int) var velocity
var fire_allowed = true
var laser = preload("res://src/space_invader/laser.tscn")

var screensize

func _ready():
	screensize = get_viewport_rect().size

func _physics_process(delta):
	var speed = Vector2()
	speed.y = 0
	if Input.is_action_pressed("ui_left"):
		speed.x -= 1
	if Input.is_action_pressed("ui_right"):
		speed.x += 1
	if Input.is_action_pressed("ui_select"):
		fire()
	speed = speed.normalized() * velocity
	global_position += speed * delta
	global_position.x = clamp(global_position.x, 0, screensize.x)
	
func fire():
	if fire_allowed == true:
		fire_allowed = false
		var laser_on = laser.instance()
		laser_on.set_pos(Vector2(position.x, position.y - $Sprite.texture.get_height() / 2 * $Sprite.scale.y - laser_on.get_node("Sprite").texture.get_height() / 2 * laser_on.get_node("Sprite").scale.y))
		var parent = get_parent()
		parent.add_child(laser_on)
		$Cadence.start()

func _on_Cadence_timeout():
	fire_allowed = true
