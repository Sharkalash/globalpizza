extends Node2D

var on_dragged = false
var select_allowed = true
var selectable = [false, false, false, false, false, false, false, false, false, false]
var nb_validate = 0
var nb_refused = 0
var label_accepted = [false, false, false, false, false, false, false, false, false, false]
var label_refused = [false, false, false, false, false, false, false, false, false, false]

func _ready():
	$Resultat.hide()
	$Lettre.play()

func _process(delta):
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and selectable[0] == true:
		$lettre.position.x += get_global_mouse_position().x - $lettre.position.x
		$lettre.position.y += get_global_mouse_position().y - $lettre.position.y
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and selectable[1] == true:
		$lettre2.position.x += get_global_mouse_position().x - $lettre2.position.x
		$lettre2.position.y += get_global_mouse_position().y - $lettre2.position.y
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and selectable[2] == true:
		$lettre3.position.x += get_global_mouse_position().x - $lettre3.position.x
		$lettre3.position.y += get_global_mouse_position().y - $lettre3.position.y
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and selectable[3] == true:
		$lettre4.position.x += get_global_mouse_position().x - $lettre4.position.x
		$lettre4.position.y += get_global_mouse_position().y - $lettre4.position.y
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and selectable[4] == true:
		$lettre5.position.x += get_global_mouse_position().x - $lettre5.position.x
		$lettre5.position.y += get_global_mouse_position().y - $lettre5.position.y
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and selectable[5] == true:
		$lettre6.position.x += get_global_mouse_position().x - $lettre6.position.x
		$lettre6.position.y += get_global_mouse_position().y - $lettre6.position.y
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and selectable[6] == true:
		$lettre7.position.x += get_global_mouse_position().x - $lettre7.position.x
		$lettre7.position.y += get_global_mouse_position().y - $lettre7.position.y
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and selectable[7] == true:
		$lettre8.position.x += get_global_mouse_position().x - $lettre8.position.x
		$lettre8.position.y += get_global_mouse_position().y - $lettre8.position.y
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and selectable[8] == true:
		$lettre9.position.x += get_global_mouse_position().x - $lettre9.position.x
		$lettre9.position.y += get_global_mouse_position().y - $lettre9.position.y
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and selectable[9] == true:
		$lettre10.position.x += get_global_mouse_position().x - $lettre10.position.x
		$lettre10.position.y += get_global_mouse_position().y - $lettre10.position.y
	
	if $lettre.global_position.x <= 500:
		label_accepted[0] = false
		label_refused[0] = true
	elif $lettre.global_position.x >= 1420:
		label_accepted[0] = true
		label_refused[0] = false
	else:
		label_accepted[0] = false
		label_refused[0] = false
	if $lettre2.global_position.x <= 500:
		label_accepted[1] = false
		label_refused[1] = true
	elif $lettre2.global_position.x >= 1420:
		label_accepted[1] = true
		label_refused[1] = false
	else:
		label_accepted[1] = false
		label_refused[1] = false
	if $lettre3.global_position.x <= 500:
		label_accepted[2] = false
		label_refused[2] = true
	elif $lettre3.global_position.x >= 1420:
		label_accepted[2] = true
		label_refused[2] = false
	else:
		label_accepted[2] = false
		label_refused[2] = false
	if $lettre4.global_position.x <= 500:
		label_accepted[3] = false
		label_refused[3] = true
	elif $lettre4.global_position.x >= 1420:
		label_accepted[3] = true
		label_refused[3] = false
	else:
		label_accepted[3] = false
		label_refused[3] = false
	if $lettre5.global_position.x <= 500:
		label_accepted[4] = false
		label_refused[4] = true
	elif $lettre5.global_position.x >= 1420:
		label_accepted[4] = true
		label_refused[4] = false
	else:
		label_accepted[4] = false
		label_refused[4] = false
	if $lettre6.global_position.x <= 500:
		label_accepted[5] = false
		label_refused[5] = true
	elif $lettre6.global_position.x >= 1420:
		label_accepted[5] = true
		label_refused[5] = false
	else:
		label_accepted[5] = false
		label_refused[5] = false
	if $lettre7.global_position.x <= 500:
		label_accepted[6] = false
		label_refused[6] = true
	elif $lettre7.global_position.x >= 1420:
		label_accepted[6] = true
		label_refused[6] = false
	else:
		label_accepted[6] = false
		label_refused[6] = false
	if $lettre8.global_position.x <= 500:
		label_accepted[7] = false
		label_refused[7] = true
	elif $lettre8.global_position.x >= 1420:
		label_accepted[7] = true
		label_refused[7] = false
	else:
		label_accepted[7] = false
		label_refused[7] = false
	if $lettre9.global_position.x <= 500:
		label_accepted[8] = false
		label_refused[8] = true
	elif $lettre9.global_position.x >= 1420:
		label_accepted[8] = true
		label_refused[8] = false
	else:
		label_accepted[8] = false
		label_refused[8] = false
	if $lettre10.global_position.x <= 500:
		label_accepted[9] = false
		label_refused[9] = true
	elif $lettre10.global_position.x >= 1420:
		label_accepted[9] = true
		label_refused[9] = false
	else:
		label_accepted[9] = false
		label_refused[9] = false

func _on_lettre_mouse_entered():
	if select_allowed == true:
		selectable[0] = true
		select_allowed = false
	
func _on_lettre_mouse_exited():
	if selectable[0] == true && not(Input.is_mouse_button_pressed(BUTTON_LEFT)):
		select_allowed = true
		selectable[0] = false
	
func _on_lettre2_mouse_entered():
	if select_allowed == true:
		selectable[1] = true
		select_allowed = false

func _on_lettre2_mouse_exited():
	if selectable[1] == true && not(Input.is_mouse_button_pressed(BUTTON_LEFT)):
		select_allowed = true
		selectable[1] = false

func _on_lettre3_mouse_entered():
	if select_allowed == true:
		selectable[2] = true
		select_allowed = false

func _on_lettre3_mouse_exited():
	if selectable[2] == true && not(Input.is_mouse_button_pressed(BUTTON_LEFT)):
		select_allowed = true
		selectable[2] = false

func _on_lettre4_mouse_entered():
	if select_allowed == true:
		selectable[3] = true
		select_allowed = false

func _on_lettre4_mouse_exited():
	if selectable[3] == true && not(Input.is_mouse_button_pressed(BUTTON_LEFT)):
		select_allowed = true
		selectable[3] = false

func _on_lettre5_mouse_entered():
	if select_allowed == true:
		selectable[4] = true
		select_allowed = false

func _on_lettre5_mouse_exited():
	if selectable[4] == true && not(Input.is_mouse_button_pressed(BUTTON_LEFT)):
		select_allowed = true
		selectable[4] = false

func _on_lettre6_mouse_entered():
	if select_allowed == true:
		selectable[5] = true
		select_allowed = false

func _on_lettre6_mouse_exited():
	if selectable[5] == true && not(Input.is_mouse_button_pressed(BUTTON_LEFT)):
		select_allowed = true
		selectable[5] = false

func _on_lettre7_mouse_entered():
	if select_allowed == true:
		selectable[6] = true
		select_allowed = false
		
func _on_lettre7_mouse_exited():
	if selectable[6] == true && not(Input.is_mouse_button_pressed(BUTTON_LEFT)):
		select_allowed = true
		selectable[6] = false

func _on_lettre8_mouse_entered():
	if select_allowed == true:
		selectable[7] = true
		select_allowed = false

func _on_lettre8_mouse_exited():
	if selectable[7] == true && not(Input.is_mouse_button_pressed(BUTTON_LEFT)):
		select_allowed = true
		selectable[7] = false

func _on_lettre9_mouse_entered():
	if select_allowed == true:
		selectable[8] = true
		select_allowed = false

func _on_lettre9_mouse_exited():
	if selectable[8] == true && not(Input.is_mouse_button_pressed(BUTTON_LEFT)):
		select_allowed = true
		selectable[8] = false

func _on_lettre10_mouse_entered():
	if select_allowed == true:
		selectable[9] = true
		select_allowed = false

func _on_lettre10_mouse_exited():
	if selectable[9] == true && not(Input.is_mouse_button_pressed(BUTTON_LEFT)):
		select_allowed = true
		selectable[9] = false

func _on_Button_pressed():
	$Resultat.show()
	$time_result.start()
	var i
	for i in range (10):
		if label_accepted[i] == true:
			nb_validate += 1
	for i in range (10):
		if label_refused[i] == true:
			nb_refused += 1
	if nb_refused == 5 and nb_validate == 5:
		var aventure = 0
		var kind = 0
		if label_accepted[0] == true:
			aventure += 1
		if label_accepted[5] == true:
			aventure += 1
		if label_accepted[7] == true:
			aventure += 1
		if label_accepted[4] == true:
			kind += 1
		if label_accepted[6] == true:
			kind += 1
		if label_accepted[8] == true:
			kind += 1
		if aventure > kind:
			$Resultat.text = "Vous êtes un Aventurier\nVous recherchez de l'animation\ncar pour vous\n le foyer est avant tout\n un lieu d'amusement"
			get_tree().set_meta("aventurer",true)
		else:
			$Resultat.text = "Vous êtes quelqu'un de Gentil\nPour vous, le foyer\ndoit être un lieu\nacceuillant dans lequel\nvous vous sentez bien"
			get_tree().set_meta("nice_guy",true)
		
		
	else:
		nb_refused = 0
		nb_validate = 0
		$Resultat.text = "You have to accept\n(put on the right)\n 5 notes\nand to refuse\n(put on the left)\nthe 5 others"

func _on_time_result_timeout():
	$Resultat.hide()
	get_tree().change_scene("res://src/game/Main.tscn")
