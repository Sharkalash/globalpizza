extends Camera2D

export (NodePath) var to_follow
export (float) var offset_x = 0
export (float) var offset_y = 0

var prec_position: Vector2

func _ready():
	pass 
	
func move(delta):
	position = 0.5 * position + 0.5 * (get_node(to_follow).position + Vector2(offset_x, offset_y))
	
func _process(delta):
	move(delta)