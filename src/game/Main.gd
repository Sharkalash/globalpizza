extends Node2D

var Man = preload("res://src/game/Man.tscn")

func start_game():
	$Man.show()
	$Music.play()

func _ready():
	
	if(get_tree().has_meta("bus") && get_tree().get_meta("bus")):
		$Music.play()
		get_tree().set_meta("bus",false)
		var score = get_tree().get_meta("scoreBus")
		
		$Man.position.x = 2100
		$Man/AnimatedSprite.flip_h = false
		
		if(score > 70):
			$Pnjbus/Pnj/LabelWin.show()
			$TimerWin.start()
			$Man.set_process(false)
			$Man/AnimatedSprite.frame = 0
			$Man/AnimatedSprite.stop()
		else:
			$Pnjbus/Pnj/LabelLoose.show()
			$Pnjbus/TimerLoose.start()
	elif(get_tree().has_meta("travel") && get_tree().get_meta("travel")):
		$EndTravel.play()
		$Man.set_process(false)
	elif(get_tree().has_meta("bar") && get_tree().get_meta("bar")):
		get_tree().set_meta("bar", false)
		$Man.position.x = 5500
	elif(get_tree().has_meta("nice_guy") and get_tree().get_meta("nice_guy")):
		start_game()
	elif(get_tree().has_meta("aventurer") and get_tree().get_meta("aventurer")):
		start_game()
		$Man.position.x = 8000
	else:
		$Man.hide()
		$Intro.play()
		visible = false
		


func _on_Pnjbus_go_for_bus():
	$Man.set_process(true)
	get_tree().set_meta("bus", true)
	get_tree().change_scene("res://src/bus/MainBus.tscn")

func _on_Pnjbus_go_bar():
	$Man.set_process(true)

func _on_TimerWin_timeout():
	$Man.set_process(false)
	$Man.play()
	

func _on_Bar_go_bar():
	get_tree().set_meta("bar", true)
	get_tree().change_scene("res://src/bar/MainBar.tscn")


func _on_Intro_finished():
	get_tree().change_scene("res://src/letter_game/lettre_game.tscn")


func _on_EndBus_finished():
	get_tree().quit()


func _on_EndTravel_finished():
	get_tree().quit()