extends Area2D

signal go_for_bus
signal go_bar

func _ready():
	position = Vector2(2000, 1080 - $Pnj.texture.get_height() - 120)
	$Pnj/Label.hide()
	$Pnj/LabelWin.hide()
	$Pnj/LabelLoose.hide()
	

func _on_Pnjbus_area_entered(area):
	$Pnj/LabelWin.hide()
	$Pnj/LabelLoose.hide()
	$Pnj/Label.show()
	area.set_process(false)
	area.get_node("AnimatedSprite").frame = 0
	area.get_node("AnimatedSprite").stop()

func _on_ButtonYes_pressed():
	emit_signal("go_for_bus")
	$Pnj/Label.hide()

func _on_ButtonNo_pressed():
	emit_signal("go_bar")
	$Pnj/Label.hide()

func _on_TimerLoose_timeout():
	$Pnj/LabelLoose.hide()
	$GoBar.play()