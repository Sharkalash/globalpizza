extends Area2D

export (int) var velocity = 400

func _ready():
	position = Vector2(0, 1080 - $AnimatedSprite.frames.get_frame("walk",0).get_height() - 70)
	$AnimatedSprite.animation = "walk"
	$AnimatedSprite.flip_h = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var speed = Vector2()
	
	if(not($AnimatedSprite.is_playing())):
		$AnimatedSprite.play()
	
	if Input.is_action_pressed("ui_left"):
		speed.x -= 1
		$AnimatedSprite.flip_h = false
	elif Input.is_action_pressed("ui_right"):
		$AnimatedSprite.flip_h = true
		speed.x += 1
	else:
		$AnimatedSprite.stop()
		$AnimatedSprite.frame = 0
		
	speed = speed.normalized() * velocity
	position += speed * delta
	
	if(position.x < 0):
		position.x = 0
		
	if(position.x >= 7000):
		$Invaders.play()
		set_process(false)


func _on_Invaders_finished():
	get_tree().change_scene("res://src/space_invader/space.tscn")